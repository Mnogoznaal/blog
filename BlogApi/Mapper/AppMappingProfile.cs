﻿using AutoMapper;
using BlogApi.DbEntities;
using BlogApi.DTO;

namespace BlogApi.Mapper
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<Post, PostDTO>().ReverseMap();
        }
    }
}
