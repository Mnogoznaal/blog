﻿using BlogApi.DTO;

namespace BlogApi.Services.Interfaces
{
    public interface IPostService
    {
        Task<bool> Create(PostDTO postDTO);
        Task<PostDTO> GetById(int id);
        Task<bool> Update(PostDTO postDTO);
        Task<bool> Delete(int id);
    }
}
