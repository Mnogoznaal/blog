﻿using AutoMapper;
using BlogApi.DbEntities;
using BlogApi.DTO;
using BlogApi.Repositories.Interfaces;
using BlogApi.Services.Interfaces;
using Microsoft.Extensions.Hosting;

namespace BlogApi.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;

        public PostService(IPostRepository postRepository, IMapper mapper)
        {
            _postRepository = postRepository;
            _mapper = mapper;
        }
        public async Task<bool> Create(PostDTO postDTO)
        {
            var post = _mapper.Map<Post>(postDTO);
            return await _postRepository.Create(post);
        }

        public async Task<bool> Delete(int id)
        {
            return await _postRepository.Delete(id);
        }

        public async Task<PostDTO> GetById(int id)
        {
            var post = await _postRepository.GetById(id);
            return _mapper.Map<PostDTO>(post);
        }

        public async Task<bool> Update(PostDTO postDTO)
        {
            var post = _mapper.Map<Post>(postDTO);
            return await _postRepository.Update(post);
        }
    }
}
