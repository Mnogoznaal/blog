﻿using BlogApi.DbEntities;
using Microsoft.EntityFrameworkCore;

namespace BlogApi.Context
{
    public class DbBlogContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbBlogContext(DbContextOptions<DbBlogContext> options) : base(options)
        {

        }
    }
}
