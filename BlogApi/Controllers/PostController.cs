﻿using BlogApi.DTO;
using BlogApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BlogApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpPost("create")]
        public async Task<ActionResult<bool>> Create([FromBody] PostDTO postDTO)
        {
            bool result = await _postService.Create(postDTO);
            return Ok(result);
        }

        [HttpPost("update")]
        public async Task<ActionResult<bool>> Update([FromBody] PostDTO postDTO)
        {
            bool result = await _postService.Update(postDTO);
            return Ok(result);
        }

        [HttpGet("get")]
        public async Task<ActionResult<PostDTO>> Get(int id)
        {
            PostDTO result = await _postService.GetById(id);
            return Ok(result);
        }

        [HttpPost("delete")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            bool result = await _postService.Delete(id);
            return Ok(result);
        }
    }
}
