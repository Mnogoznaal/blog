﻿using BlogApi.DbEntities;

namespace BlogApi.Repositories.Interfaces
{
    public interface IPostRepository
    {
        Task<bool> Create(Post post);
        Task<Post> GetById(int id);
        Task<bool> Update(Post post);
        Task<bool> Delete(int id);
    }
}
