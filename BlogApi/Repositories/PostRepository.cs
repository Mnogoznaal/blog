﻿using BlogApi.Context;
using BlogApi.DbEntities;
using BlogApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BlogApi.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly DbBlogContext _dbBlogContext;
        public PostRepository(DbBlogContext dbBlogContext)
        {
            _dbBlogContext = dbBlogContext;
        }
        public async Task<bool> Create(Post post)
        {
            try
            {
                await _dbBlogContext.Posts.AddAsync(post);
                await _dbBlogContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var post = await _dbBlogContext.Posts.FirstOrDefaultAsync(x => x.Id == id);
            if (post != null)
            {
                _dbBlogContext.Posts.Remove(post);
                await _dbBlogContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<Post> GetById(int id)
        {
            return await _dbBlogContext.Posts.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> Update(Post post)
        {
            try
            {
                _dbBlogContext.Posts.Update(post);
                await _dbBlogContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
